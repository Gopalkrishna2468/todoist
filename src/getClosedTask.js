const fetch = require('node-fetch');
const readline = require('readline');

require('dotenv').config()
const TOKEN = process.env.TOKEN

function getClosedTask(closeDataId) {
    try {
        const r1 = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        })
        r1.question('Are you sure to close the task?', (answer) => {
            var deleted = true;
            if (answer == 'y' || answer == 'Y') {
                let response = fetch(`https://api.todoist.com/rest/v1/tasks/${closeDataId}/close`, {
                    method: "POST",
                    headers: {
                        Authorization: `Bearer ${TOKEN}`,
                    },
                });
                console.log('Task Closed')
                r1.close()
            } else {
                r1.close()
            }
        })

    } catch (err) {
        console.log(err)
    }

}

module.exports=getClosedTask;