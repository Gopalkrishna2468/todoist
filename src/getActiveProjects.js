const fetch = require('node-fetch');

require('dotenv').config()
const TOKEN = process.env.TOKEN


async function getActiveProjects() {
    try {
        let projectData = await fetch('https://api.todoist.com/rest/v1/projects', {
                headers: {
                    Authorization: `Bearer ${TOKEN}`,
                },
            }).then(res => res.json())
            .then(data => {
                return data
            })
        console.table(projectData)
    } catch {
        (err) => {
            console.log(err)
        }
    }
}

module.exports = getActiveProjects;