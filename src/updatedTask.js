const fetch = require('node-fetch');

require('dotenv').config()
const TOKEN = process.env.TOKEN


async function getUpdatedTask(updateData, dataId) {
    try {
        let content = {
            "content": `${updateData}`,
            "due_string": "tomorrow at 12:00",
            "due_lang": "en",
            "priority": 4,
        }
        let updatedTaskData = await fetch(`https://api.todoist.com/rest/v1/tasks/${dataId}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${TOKEN}`,
            },
            body: JSON.stringify(content)
        })
        if (updatedTaskData.status == 204) {
            console.log('Task Updated')
        }
    } catch (err) {
        console.log(err)
    }
}
module.exports = getUpdatedTask;