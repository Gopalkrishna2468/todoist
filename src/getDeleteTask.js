const fetch = require('node-fetch');
const readline = require('readline');

require('dotenv').config()
const TOKEN = process.env.TOKEN
function getDeletedTask(deleteData) {
    try {
        const r1 = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        })
        r1.question('Are you sure to delete the task?', (answer) => {
            var deleted = true;
            if (answer == 'y' || answer == 'Y') {
                let deleteTaskData = fetch(`https://api.todoist.com/rest/v1/tasks/${deleteData}`, {
                    method: "DELETE",
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${TOKEN}`,
                    },
                })
                console.log('Task Deleted')
                r1.close()
            } else {
                r1.close()
            }
        })

    } catch (err) {
        console.log(err)
    }

}
module.exports=getDeletedTask;