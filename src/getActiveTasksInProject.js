const fetch = require('node-fetch');
require('dotenv').config()
const TOKEN = process.env.TOKEN


async function getActiveTasksInProject() {
    try {
        let taskData = await fetch('https://api.todoist.com/rest/v1/tasks?project_id=2270794857', {
                headers: {
                    Authorization: `Bearer ${TOKEN}`,
                },
            }).then(res => res.json())
            .then(data => {
                return data
            })
        console.table(taskData)
    } catch {
        (err) => {
            console.log(err)
        }
    }
}
module.exports=getActiveTasksInProject;