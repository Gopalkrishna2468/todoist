const fetch = require('node-fetch');
const readlineSync = require('readline-sync');
require('dotenv').config()
const TOKEN = process.env.TOKEN

async function updateTasksInProject() {
    try {
        var updateData = readlineSync.question('Enter the TaskName:')
        var dataId = readlineSync.question('Enter the TaskId:')
        let content = {
            "content": `${updateData}`,
            "due_string": "tomorrow at 12:00",
            "due_lang": "en",
            "priority": 4,
            "project_id":2270794857
        }
        let updatedTaskData = await fetch(`https://api.todoist.com/rest/v1/tasks/${dataId}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${TOKEN}`,
            },
            body: JSON.stringify(content)
        })
        if (updatedTaskData.status == 204) {
            console.log('Task Updated')
        }
    } catch (err) {
        console.log(err)
    }
}
module.exports=updateTasksInProject;