const fetch = require('node-fetch');
const readlineSync = require('readline-sync');
require('dotenv').config()
const TOKEN = process.env.TOKEN

async function addNewTask() {
    try {
        var contentData = readlineSync.question('Enter the NewTask:')
        var timeData = readlineSync.question('Enter the TaskTimings:')
        var description = readlineSync.question('Enter the TaskDescription:')
        let content = {
            "content": `${contentData}`,
            "due_string": `${timeData}`,
            "description": `${description}`,
            "priority": 4,
        }
        let taskData = await fetch('https://api.todoist.com/rest/v1/tasks', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${TOKEN}`,
            },
            body: JSON.stringify(content)
        })
        console.log(taskData)
    } catch {
        (err) => {
            console.log(err)
        }
    }
}
module.exports = addNewTask;