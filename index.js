const yargs = require('yargs');
const argv = yargs
    .command('ls', 'lists all projects of todoist')
    .command('activeTask', 'lists all active tasks')
    .command('add', 'Updates the tasks')
    .command('close', 'closes the task')
    .command('delete', 'delete the task')
    .command('update', 'update the task')
    .help()
    .option('find')
    .alias('help', 'h')
    .argv

let closedTask = require('./src/closedTask');
let deletedTask = require('./src/deleteTask');
let getActiveProjects = require('./src/getActiveProjects')
let getActiveTasks = require('./src/getActiveTasks')
let addNewTask = require('./src/addNewTask')
let updatedTask = require('./src/updateTask')
let getActiveTasksInProject=require('./src/getActiveTasksInProject')
let updateTasksInProject=require('./src/updateTaskInProject')
let addTaskInProject=require('./src/addTaskInProject')
let closedTaskInProject=require('./src/closeTaskInProject')
let deletedTaskInProject=require('./src/deleteTaskInProject')




if (argv._[0] === 'ls') {
    getActiveProjects = getActiveProjects();
} else if (argv._[0] === 'active') {
    getActiveTasks = getActiveTasks();
} else if (argv._[0] === 'add') {
        addNewTask = addNewTask();
} else if (argv._[0] === 'close') {
        closedTask = closedTask();
} else if (argv._[0] === 'delete') {
    if(argv.id!=undefined){ deletedTask = deletedTask(argv.id);}
       else{
           console.log('Please enter valid Id')
       }
} else if (argv._[0] === 'update') {
        updatedTask = updatedTask();
   



} else if (argv._[0] === 'addProject') {
    addNewProject();
}   else if (argv._[0] === 'lspro') {
    getActiveTasksInProject=getActiveTasksInProject();
}else if (argv._[0] === 'updatepro') {
    updateTasksInProject=updateTasksInProject();
}
else if (argv._[0] === 'addpro') {
    addTaskInProject=addTaskInProject();
}
else if (argv._[0] === 'closepro') {
    closedTaskInProject=closedTaskInProject();
} 
else if (argv._[0] === 'deletepro') {
    deletedTaskInProject=deletedTaskInProject();
} else {
    console.log('invalid command')
}